import Link from "next/link"
import { useRouter } from "next/router"

const article = ({ article }) => {
    return (
        <div className="w-4/5 mx-auto my-8">
            <h1>
                {article.title} - Article {article.id}
            </h1>
            <p>{article.body}</p>
            <Link href="/">Back</Link>
        </div>
    )
}

// export const getServerSideProps = async (context) => {
//     const res = await fetch(
//         `https://jsonplaceholder.typicode.com/posts/${context.params.id}`
//     )
//     const article = await res.json()

//     return {
//         props: { article },
//     }
// }

export const getStaticProps = async (context) => {
    const res = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${context.params.id}`
    )
    const article = await res.json()

    return {
        props: { article },
    }
}

export const getStaticPaths = async () => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts`)
    const articles = await res.json()

    const ids = articles.map((article) => article.id)
    const paths = ids.map((id) => ({ params: { id: id.toString() } }))

    return {
        paths,
        fallback: false,
    }
}

export default article
