import Head from "next/head"
import Image from "next/image"
import ArticleList from "../components/ArticleList"
import Header from "../components/Header"

export default function Home({ articles }) {
    return (
        <>
            <Head>
                <title>Ayy</title>
            </Head>
            <Header />
            <div className="w-full">
                <ArticleList articles={articles} />
            </div>
        </>
    )
}

export const getStaticProps = async () => {
    const res = await fetch(
        `https://jsonplaceholder.typicode.com/posts?_limit=8`
    )
    const articles = await res.json()

    return {
        props: {
            articles,
        },
    }
}
