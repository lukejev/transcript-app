import ArticleItem from "./ArticleItem"

const ArticleList = ({ articles }) => {
    return (
        <div className="flex justify-center p-4 gap-4 flex-wrap w-4/5 m-auto">
            {articles.map((article, key) => (
                <ArticleItem key={key} article={article}></ArticleItem>
            ))}
        </div>
    )
}

export default ArticleList
