import Link from "next/link"

const ArticleItem = ({ article }) => {
    return (
        <div className="border rounded-sm p-2 w-64">
            <Link href="/article/[id]" as={`/article/${article.id}`} passHref>
                <a>
                    <p className="italic text-lg">{article.title}</p>
                    <p>~</p>
                    <p>{article.body}</p>
                </a>
            </Link>
        </div>
    )
}

export default ArticleItem
