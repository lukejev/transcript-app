import Link from "next/link"

const Nav = () => {
    return (
        <nav className="bg-black text-white p-4">
            <ul className="flex gap-4">
                <li className="list-none">
                    <Link href="/">Home</Link>
                </li>
                <li className="list-none">
                    <Link href="/about">About</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Nav
